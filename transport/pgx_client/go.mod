module gitlab.wm.local/wm/pkg/transport/pgx_client

go 1.21

require (
	github.com/jackc/pgx/v5 v5.6.0
	github.com/pashagolub/pgxmock/v3 v3.4.0
	gitlab.wm.local/wm/pkg/utils v0.4.2
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	golang.org/x/crypto v0.26.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/text v0.17.0 // indirect
)
