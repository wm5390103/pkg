module gitlab.wm.local/wm/pkg/mail/imap

go 1.21

require (
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-imap-quota v0.0.0-20210203125329-619074823f3c
	github.com/emersion/go-imap-sortthread v1.2.0
	github.com/emersion/go-imap-uidplus v0.0.0-20200503180755-e75854c361e9
)

require (
	github.com/emersion/go-sasl v0.0.0-20241020182733-b788ff22d5a6 // indirect
	golang.org/x/text v0.20.0 // indirect
)
