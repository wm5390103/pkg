module gitlab.wm.local/wm/pkg/mail/smtpd

go 1.21

require (
	gitlab.wm.local/wm/pkg/log v0.7.6
	gitlab.wm.local/wm/pkg/utils v0.4.2
)

require (
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
