package go_webmoney_files

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type bulkRequest struct {
	Operation string   `json:"operation"`
	FolderIDs []string `json:"folder_ids"`
	FileIDs   []string `json:"file_ids"`
	ToFolder  string   `json:"to_folder"`
}
type copyResponse struct {
	Files   []*File   `json:"files"`
	Folders []*Folder `json:"folders"`
}

type Folder struct {
	Contents []Contents   `json:"contents"`
	Object   FolderObject `json:"object"`
}

type Contents struct {
	ID                string      `json:"id"`
	Name              string      `json:"name"`
	Description       string      `json:"description"`
	ContentType       string      `json:"content_type"`
	Shared            bool        `json:"shared"`
	Size              int         `json:"size"`
	Sha1              string      `json:"sha1"`
	Crc32             string      `json:"crc32"`
	ParentID          string      `json:"parent_id"`
	SenderID          interface{} `json:"sender_id"`
	UserID            string      `json:"user_id"`
	CreatedAt         time.Time   `json:"created_at"`
	UpdatedAt         time.Time   `json:"updated_at"`
	IsDir             bool        `json:"is_dir"`
	Icon              string      `json:"icon"`
	Preview           bool        `json:"preview"`
	SecureURL         string      `json:"secure_url"`
	Metadata          interface{} `json:"metadata"`
	LastInteractionAt time.Time   `json:"last_interaction_at"`
	Unread            bool        `json:"unread"`
	Inline            bool        `json:"inline"`
	Owner             Owner       `json:"owner"`
}

type Owner Correspondent
type Ancestry struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	NameShort string `json:"name_short"`
}
type FolderObject struct {
	ID             string      `json:"id"`
	Name           string      `json:"name"`
	Shared         bool        `json:"shared"`
	ParentID       interface{} `json:"parent_id"`
	UserID         string      `json:"user_id"`
	CreatedAt      time.Time   `json:"created_at"`
	UpdatedAt      time.Time   `json:"updated_at"`
	IsDir          bool        `json:"is_dir"`
	SecureURL      string      `json:"secure_url"`
	WhiddenFolders bool        `json:"whidden_folders"`
	WhiddenFiles   bool        `json:"whidden_files"`
	Ancestry       []Ancestry  `json:"ancestry"`
	Owner          Owner       `json:"owner"`
}

func (f *Client) FolderList(folder string) (*Folder, error) {
	var url string
	if folder == "" {
		url = "folders.json"
	} else {
		url = "/folders/" + folder + ".json"
	}
	result, err := f.SendGetRequest(url)
	if err != nil {
		return nil, err
	}
	resultFolder := &Folder{}
	json.Unmarshal(result, resultFolder)
	return resultFolder, nil
}

func (f *Client) FolderCreate(name, parentID string) (*Folder, error) {
	res, err := f.SendRequest(http.MethodPost, "folders.json", []byte(fmt.Sprintf("{\"folder\": { \"name\": \"%s\", \"parent_id\":\"%s\"}}", name, parentID)))
	if err != nil {
		return nil, err
	}
	resultFolder := &Folder{}
	json.Unmarshal(res, resultFolder)
	return resultFolder, nil

}

func (f *Client) FolderRename(name, folderID string) (*Folder, error) {
	res, err := f.SendRequest(http.MethodPatch,
		"folders/"+folderID+".json",
		[]byte(fmt.Sprintf("{\"folder\": {\"name\": \"%s\"} }", name)))
	if err != nil {
		return nil, err
	}
	folder := &Folder{}
	err = json.Unmarshal(res, folder)
	if err != nil {
		return nil, err
	}
	return folder, nil
}

func (f *Client) FolderMove(folderIDs, fileIDs []string, toFolder string) error {
	request := &bulkRequest{
		Operation: "move",
		FolderIDs: folderIDs,
		FileIDs:   fileIDs,
		ToFolder:  toFolder,
	}
	marshal, err := json.Marshal(request)
	if err != nil {
		return err
	}
	_, err = f.SendRequest(http.MethodPatch, "folders/bulk.json", marshal)
	if err != nil {
		return err
	}
	return nil
}

func (f *Client) FolderCopy(folderIDs, fileIDs []string, toFolder string) ([]*File, []*Folder, error) {
	request := &bulkRequest{
		Operation: "copy",
		FolderIDs: folderIDs,
		FileIDs:   fileIDs,
		ToFolder:  toFolder,
	}
	marshal, err := json.Marshal(request)
	if err != nil {
		return nil, nil, err
	}
	res, err := f.SendRequest(http.MethodPatch, "folders/bulk.json", marshal)
	if err != nil {
		return nil, nil, err
	}
	copyRes := &copyResponse{}
	err = json.Unmarshal(res, copyRes)
	if err != nil {
		return nil, nil, err
	}
	return copyRes.Files, copyRes.Folders, nil
}

func (f *Client) FolderMarkAsRead() error {
	_, err := f.SendRequest(http.MethodPatch, "/folders/bulk.json", []byte("{\"operation\": \"mark_as_read\"}"))
	if err != nil {
		return err
	}
	return nil
}
func (f *Client) FolderDelete(folderIDs, fileIDs []string) error {
	request := &bulkRequest{
		Operation: "delete",
		FolderIDs: folderIDs,
		FileIDs:   fileIDs,
	}
	marshal, err := json.Marshal(request)
	if err != nil {
		return err
	}
	_, err = f.SendRequest(http.MethodPatch, "/folders/bulk.json", marshal)
	if err != nil {
		return err
	}
	return nil
}

func (f *Client) FolderShare(folderID string, allowWmid []string, shared bool) (*Folder, error) {
	wmids, err := json.Marshal(allowWmid)
	if err != nil {
		return nil, err
	}
	js := []byte(fmt.Sprintf("{\"folder\": {\"shared\": %t, \"allowed_wmids\": %s }}", shared, wmids))
	fmt.Printf("%s", js)
	res, err := f.SendRequest(http.MethodPatch, "folders/"+folderID+".json", js)

	if err != nil {
		return nil, err
	}
	folder := &Folder{}
	err = json.Unmarshal(res, folder)
	if err != nil {
		return nil, err
	}
	return folder, nil
}

func (f *Client) FolderDownload() {
	panic("method not found")
}
