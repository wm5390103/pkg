package go_webmoney_files

import (
	"encoding/json"
	"net/http"
)

type Account struct {
	ID                         string `json:"id"`
	LimitCount                 int    `json:"limit_count"`
	LimitSize                  int64  `json:"limit_size"`
	VolumeCount                int    `json:"volume_count"`
	VolumeSize                 int    `json:"volume_size"`
	PrefersTimeZone            string `json:"prefers_time_zone"`
	PrefersWebdavEnabled       bool   `json:"prefers_webdav_enabled"`
	PrefersShowHidden          bool   `json:"prefers_show_hidden"`
	PrefersDisableUnauthSender bool   `json:"prefers_disable_unauth_sender"`
	PrefersShowHiddenWebdav    bool   `json:"prefers_show_hidden_webdav"`
	Wmid                       string `json:"wmid"`
	Avatar                     string `json:"avatar"`
	UnreadCount                int    `json:"unread_count"`
}

type UserPref struct {
	PrefersTimeZone            string `json:"prefers_time_zone"`
	PrefersWebdavEnabled       bool   `json:"prefers_webdav_enabled"`
	PrefersShowHidden          bool   `json:"prefers_show_hidden"`
	PrefersDisableUnauthSender bool   `json:"prefers_disable_unauth_sender"`
	PrefersShowHiddenWebdav    bool   `json:"prefers_show_hidden_webdav"`
}

type Correspondents struct {
	Correspondents []*Correspondent `json:"correspondents"`
}

type Correspondent struct {
	Name   string `json:"name"`
	Wmid   string `json:"wmid"`
	Avatar string `json:"avatar"`
}

func (f *Client) Account() (*Account, error) {
	result, err := f.SendGetRequest("account.json")
	if err != nil {
		return nil, err
	}
	account := &Account{}
	err = json.Unmarshal(result, account)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (f *Client) AccountUpdate(pref *UserPref) (*Account, error) {
	marshal, err := json.Marshal(pref)
	if err != nil {
		return nil, err
	}
	res, err := f.SendRequest(http.MethodPatch, "account.json", marshal)
	if err != nil {
		return nil, err
	}
	ac := &Account{}
	err = json.Unmarshal(res, ac)
	if err != nil {
		return nil, err
	}
	return ac, nil
}

func (f *Client) Correspondents() (*Correspondents, error) {
	res, err := f.SendGetRequest("/account/correspondents.json")
	if err != nil {
		return nil, err
	}
	correspondents := &Correspondents{}
	err = json.Unmarshal(res, correspondents)
	if err != nil {
		return nil, err
	}
	return correspondents, err

}
