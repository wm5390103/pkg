module gitlab.wm.local/wm/pkg/files/api_client

go 1.19

require (
	github.com/C0nstantin/go-webmoney v1.7.9
	gitlab.wm.local/wm/pkg/utils v0.4.2
)

require golang.org/x/crypto v0.26.0 // indirect
