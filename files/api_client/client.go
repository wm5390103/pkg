package go_webmoney_files

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/C0nstantin/go-webmoney/wmsigner"
	"gitlab.wm.local/wm/pkg/utils"
	"io"
	"mime/multipart"
	"net/http"
	u "net/url"
	"os"
	"path/filepath"
)

var (
	defaultBasePath = "https://files.web.money"
)

type Signer interface {
	Sign(str string) (string, error)
}

type sesRes struct {
	Session struct {
		SessionID string `json:"session_id"`
	} `json:"session"`
}

type Client struct {
	session  string
	signer   Signer
	basePath string
	wmid     string
}

func (f *Client) Auth() error {

	resp, err := http.Get(f.urlRequest("login.json"))
	if err != nil {
		return err
	}
	result, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	s := &sesRes{}
	err = json.Unmarshal(result, s)
	if err != nil {
		return err
	}
	sign, err := f.signer.Sign(s.Session.SessionID + f.wmid)
	if err != nil {
		return err
	}
	body := bytes.NewBuffer([]byte(fmt.Sprintf("{\"wmid\": \"%s\", \"sign\": \"%s\"}", f.wmid, sign)))
	authReq, err := http.NewRequest(http.MethodPost, f.urlRequest("authenticate.json"), body)
	if err != nil {
		return err
	}
	authReq.Header.Add("Content-Type", "application/json; charset=UTF-8")
	authReq.Header.Add("Accept", "application/json, text/plain, */*")

	authReq.AddCookie(&http.Cookie{Value: s.Session.SessionID, Name: "session_id"})
	c := &http.Client{}
	responseAuth, err := c.Do(authReq)
	if err != nil {
		return err
	}
	respBody, err := io.ReadAll(responseAuth.Body)
	if err != nil {
		return err
	}
	if responseAuth.StatusCode != http.StatusOK {
		return errors.New(" invalid request in auth " + string(respBody))
	}

	resultSession := &sesRes{}
	err = json.Unmarshal(respBody, resultSession)
	if err != nil {
		return err
	}
	f.session = resultSession.Session.SessionID
	return nil
}

func (f *Client) uploadRequest(pathFile, folderID string) ([]byte, error) {
	var requestBody bytes.Buffer
	file, err := os.Open(pathFile)
	if err != nil {
		return nil, err
	}
	multipartWriter := multipart.NewWriter(&requestBody)
	fwr, err := multipartWriter.CreateFormFile("file", filepath.Base(pathFile))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(fwr, file)
	if err != nil {
		return nil, err
	}

	utils.DeferCloseLog(multipartWriter) // close multipart writer before send send request
	req, _ := http.NewRequest(http.MethodPost,
		f.urlRequest("/fast_upload?preprocess=false&format=json&parent_id="+folderID), &requestBody)
	req.AddCookie(&http.Cookie{Name: "session_id", Value: f.session})
	req.Header.Set("Content-Type", multipartWriter.FormDataContentType())
	c := &http.Client{}
	res, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return []byte{}, errors.New("error request " + string(resBody))
	}

	return resBody, nil

}

func (f *Client) SendRequest(method, url string, body []byte) ([]byte, error) {
	req, err := http.NewRequest(method, f.urlRequest(url), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("Accept", "application/json, text/plain, */*")
	req.AddCookie(&http.Cookie{Name: "session_id", Value: f.session})
	c := &http.Client{}
	res, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	ResBody, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return []byte{}, errors.New("error request " + string(ResBody))
	}

	return ResBody, nil
}

func (f *Client) SendGetRequest(url string) ([]byte, error) {
	return f.SendRequest(http.MethodGet, url, nil)
}

func (f *Client) urlRequest(url string) string {
	Base, _ := u.Parse(f.basePath)
	Url, _ := u.Parse(url)
	return Base.ResolveReference(Url).String()
}

func NewClient(wmid, pass, key, basePath string) (*Client, error) {
	if basePath == "" {
		basePath = defaultBasePath
	}
	f := &Client{
		signer:   wmsigner.NewSigner(wmid, pass, key),
		basePath: basePath,
		wmid:     wmid,
	}
	err := f.Auth()
	if err != nil {
		return nil, err
	}
	return f, nil

}
