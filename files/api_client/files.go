package go_webmoney_files

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type File struct {
	Code            int         `json:"code"`
	ID              string      `json:"id"`
	DataFileName    string      `json:"dataFileName"`
	DataFileSize    int         `json:"dataFileSize"`
	DataContentType string      `json:"dataContentType"`
	FolderID        string      `json:"folderId"`
	UploadTicket    interface{} `json:"uploadTicket"`
	Versions        interface{} `json:"versions"`
	Object          FileObject  `json:"object"`
}

type FileObject struct {
	ID                string      `json:"id"`
	Name              string      `json:"name"`
	Description       string      `json:"description"`
	ContentType       string      `json:"content_type"`
	Shared            bool        `json:"shared"`
	Size              int         `json:"size"`
	Sha1              string      `json:"sha1"`
	Crc32             string      `json:"crc32"`
	ParentID          string      `json:"parent_id"`
	SenderID          interface{} `json:"sender_id"`
	UserID            string      `json:"user_id"`
	CreatedAt         time.Time   `json:"created_at"`
	UpdatedAt         time.Time   `json:"updated_at"`
	IsDir             bool        `json:"is_dir"`
	Icon              string      `json:"icon"`
	Preview           bool        `json:"preview"`
	SecureURL         string      `json:"secure_url"`
	Metadata          interface{} `json:"metadata"`
	LastInteractionAt time.Time   `json:"last_interaction_at"`
	Unread            bool        `json:"unread"`
	Inline            bool        `json:"inline"`
	Owner             Owner       `json:"owner"`
}

type UpdateRequest struct {
	File UpdateFile `json:"file"`
}
type Geo struct {
	Acc int     `json:"acc"`
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}
type Metadata struct {
	Geo  Geo    `json:"geo"`
	Info string `json:"info"`
}
type UpdateFile struct {
	Name        string   `json:"name,omitempty"`
	Description string   `json:"description,omitempty"`
	Metadata    Metadata `json:"metadata,omitempty"`
}
type MassiveRequest struct {
	Operation   string   `json:"operation"`
	FileIds     []string `json:"file_ids"`
	ToWmid      []string `json:"to_wmid"`
	WithPrepaid bool     `json:"with_prepaid"`
	Invoice     Invoice  `json:"invoice"`
	Quiet       string   `json:"quiet"`
}
type Invoice struct {
	Amount  string `json:"amount"`
	WmPurse string `json:"wm_purse"`
}

type Recipient struct {
	Wmid  string  `json:"wmid"`
	Files []*File `json:"files"`
}
type MassiveResponse struct {
	Recipients []*Recipient `json:"recipients"`
}

func (f *Client) FileInfo(fileID string) (*File, error) {
	res, err := f.SendGetRequest("/files/" + fileID + ".json")
	if err != nil {
		return nil, err
	}
	file := &File{}
	if err = json.Unmarshal(res, file); err != nil {
		return nil, err
	}
	return file, nil
}

func (f *Client) FileUpload(filePath, folderID string) (*File, error) {
	resp, err := f.uploadRequest(filePath, folderID)
	if err != nil {
		return nil, fmt.Errorf("%w | response: %s", err, resp)
	}
	file := &File{}
	err = json.Unmarshal(resp, file)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func (f *Client) FileUpdate(fileID string, up *UpdateRequest) (*File, error) {
	reqBody, err := json.Marshal(up)
	if err != nil {
		return nil, err
	}
	resp, err := f.SendRequest(http.MethodPatch, "/files/"+fileID+".json", reqBody)

	if err != nil {
		return nil, err
	}
	resultFile := &File{}
	err = json.Unmarshal(resp, resultFile)
	if err != nil {
		return nil, err
	}
	return resultFile, nil
}

func (f *Client) FileSendTo(fileIDs, wmids []string) (*MassiveResponse, error) {
	m := &MassiveRequest{
		Operation:   "send_to",
		FileIds:     fileIDs,
		ToWmid:      wmids,
		WithPrepaid: false,
		Invoice:     Invoice{},
		Quiet:       "1",
	}
	return f.FileMassive(m)

}

func (f *Client) FileShared(fileID string, shared bool) (*File, error) {
	requestBody := []byte(fmt.Sprintf("{\"file\":{\"shared\": %t}}", shared))
	res, err := f.SendRequest(http.MethodPatch, "/files/"+fileID+".json", requestBody)
	if err != nil {
		return nil, err
	}
	resultFile := &File{}
	err = json.Unmarshal(res, resultFile)
	if err != nil {
		return nil, err
	}
	return resultFile, nil
}

func (f *Client) FileMassive(massive *MassiveRequest) (*MassiveResponse, error) {
	reqBody, err := json.Marshal(massive)
	if err != nil {
		return nil, err
	}

	response, err := f.SendRequest(http.MethodPut, "/files/massive.json", reqBody)
	if err != nil {
		return nil, err
	}
	mResp := &MassiveResponse{}
	err = json.Unmarshal(response, mResp)
	if err != nil {
		return nil, err
	}
	return mResp, nil
}
