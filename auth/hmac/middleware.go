package hmac

import (
	"errors"
	"gitlab.wm.local/wm/pkg/log"

	"github.com/gin-gonic/gin"
)

var ErrAuthHeaderNotFound = errors.New("authorization header not found")
var ErrRequestUnAuthorized = errors.New("request not authorize")
var ErrAuthHeaderInvalid = errors.New("invalid format auth header")
var ErrMethodNotSupported = errors.New("hash method not supported")
var ErrAccessIDNotSet = errors.New("access id not set")

type AuthMiddleware struct {
	HmacAuth APIAuth
}

func (h AuthMiddleware) AuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		err := func(ctx *gin.Context) error {
			return h.HmacAuth.CheckRequest(ctx.Request)
		}(ctx)
		if err != nil {
			_ = ctx.Error(err)
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}

type AuthMultiMiddleware struct {
	Auths map[string]APIAuth
}

func (h AuthMultiMiddleware) AuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		signParams, err := parseHeaders(ctx.Request)
		if err != nil {
			log.Errorf("Auth error, parseHeaders %+v", err)
			_ = ctx.Error(err)
			ctx.Abort()
			return
		}
		auth, ok := h.Auths[signParams.accessID]
		if !ok {
			log.Errorf("Auth error, accessID not found")
			_ = ctx.Error(ErrAccessIDNotSet)
			ctx.Abort()
			return
		}
		err = auth.CheckRequest(ctx.Request)
		if err != nil {
			_ = ctx.Error(err)
			ctx.Abort()
			return
		}

		ctx.Next()
	}
}
func NewAuthMultiMiddleware(a map[string][]byte) AuthMultiMiddleware {
	auths := make(map[string]APIAuth)
	for accessID, secretKey := range a {
		auths[accessID] = APIAuth{SecretKey: secretKey, AccessID: accessID, EncodeBase64: true}
	}
	return AuthMultiMiddleware{
		Auths: auths,
	}
}
