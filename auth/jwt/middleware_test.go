package jwt

import (
	"net/http/httptest"
	"reflect"
	"testing"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

func TestNewGinJWTMiddleware(t *testing.T) {
	j, err := NewGinJWTMiddleware([]byte("asfasdf"))
	if j != nil || err == nil {
		t.Errorf("should fail with invalid key")
	}

	j2, err2 := NewGinJWTMiddleware([]byte("asdfasdfasdfasdfasdwerwqresdfasdfasdfasdfasqwrqwerfasdadsfasdfasdfadsfasdfasdfasdfadsfadsfasdfasdfadsfasdfasdfasdfasdfasdfadsfadsfasdfasdfasdf"))
	if j2 == nil || err2 != nil {
		t.Errorf("should not fail with valid key")
	}
	//assert.NoError(t, err2)
	if reflect.TypeOf(j2) != reflect.TypeOf(&jwt.GinJWTMiddleware{}) {
		t.Errorf("should be of type *jwt.GinJWTMiddleware")
	}
}

func Test_isJson(t *testing.T) {
	tr := []string{
		"application/json",
		"text/json",
		"text/x-json",
	}
	fs := []string{
		"application/x-javascript",
		"text/x-javascript",
		"application/ogg",
		"application/zip",
	}
	for _, trr := range tr {
		c, _ := gin.CreateTestContext(httptest.NewRecorder())
		c.Request = httptest.NewRequest("GET", "/", nil)
		c.Request.Header.Add("Content-Type", trr)
		if !isJSON(c) {
			t.Errorf("request body is not json")
		}
	}
	for _, trr := range fs {
		c, _ := gin.CreateTestContext(httptest.NewRecorder())
		c.Request = httptest.NewRequest("GET", "/", nil)
		c.Request.Header.Add("Content-Type", trr)
		if isJSON(c) {
			t.Errorf("request body should not be json")
		}
	}

}
