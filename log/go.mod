module gitlab.wm.local/wm/pkg/log

go 1.19

require github.com/sirupsen/logrus v1.9.3

require (
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
)
