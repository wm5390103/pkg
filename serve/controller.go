package serve

import (
	"github.com/gin-gonic/gin"
)

type BaseController struct {
}

func (b *BaseController) GET(r gin.IRoutes, p string, f func(ctx *gin.Context) error, h ...gin.HandlerFunc) {
	h = append(h, func(ctx *gin.Context) {
		if err := f(ctx); err != nil {
			b.Abort(ctx, err)
		}
	})
	r.GET(p, h...)
}

func (b *BaseController) POST(r gin.IRoutes, p string, f func(ctx *gin.Context) error, h ...gin.HandlerFunc) {
	h = append(h, func(ctx *gin.Context) {
		if err := f(ctx); err != nil {
			b.Abort(ctx, err)
		}
	})
	r.POST(p, h...)
}

func (b *BaseController) PUT(r gin.IRoutes, p string, f func(ctx *gin.Context) error, h ...gin.HandlerFunc) {
	h = append(h, func(ctx *gin.Context) {
		if err := f(ctx); err != nil {
			b.Abort(ctx, err)
		}
	})
	r.PUT(p, h...)
}

func (b *BaseController) PATCH(r gin.IRoutes, p string, f func(ctx *gin.Context) error, h ...gin.HandlerFunc) {
	h = append(h, func(ctx *gin.Context) {
		if err := f(ctx); err != nil {
			b.Abort(ctx, err)
		}
	})
	r.PATCH(p, h...)
}

func (b *BaseController) DELETE(r gin.IRoutes, p string, f func(ctx *gin.Context) error, h ...gin.HandlerFunc) {
	h = append(h, func(ctx *gin.Context) {
		if err := f(ctx); err != nil {
			b.Abort(ctx, err)
		}
	})
	r.DELETE(p, h...)
}

func (b *BaseController) Abort(ctx *gin.Context, err error) {
	ctx.Abort()
	_ = ctx.Error(err)
}
