package serve

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	"syscall"

	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/log"
)

var (
	ErrEmptyRequestParams = &ResponseError{
		HTTPCode: http.StatusBadRequest,
		Reason:   "empty_request_params",
		Message:  "empty request params",
	}
	ErrInvalidRequestParams = &ResponseError{
		HTTPCode: http.StatusBadRequest,
		Reason:   "invalid_request_params",
		Message:  "invalid request params",
	}
	ErrDefaultBadRequest = &ResponseError{
		HTTPCode: http.StatusBadRequest,
		Reason:   "bad_request",
		Message:  "bad request",
	}
	ErrDefaultInternalError = &ResponseError{
		HTTPCode: http.StatusInternalServerError,
		Reason:   "internal_error",
		Message:  "internal error",
	}
	ErrDefaultNotFound = &ResponseError{
		HTTPCode: http.StatusNotFound,
		Reason:   "not_found",
		Message:  "not found",
	}
	ErrDefaultForbidden = &ResponseError{
		HTTPCode: http.StatusForbidden,
		Reason:   "forbidden",
		Message:  "forbidden",
	}
	ErrDefaultUnauthorized = &ResponseError{
		HTTPCode: http.StatusUnauthorized,
		Reason:   "unauthorized",
		Message:  "unauthorized",
	}
)

type ResponseError struct {
	XMLName  xml.Name `json:"-" xml:"response"`
	HTTPCode int      `json:"code" xml:"code"`
	Reason   string   `json:"reason" xml:"reason"`
	Message  string   `json:"message" xml:"message"`
}

func (e *ResponseError) Error() string {
	return e.Message
}

type HttpErrorHandler struct {
	CustomErrors map[error]*ResponseError
	//CustomTypeErrors map[interface{}]*ResponseError
	IgnoreErrors []error
	//IgnoreTypeErrors []interface{}
	Logger       log.Logger
	ReturnError  func(ctx *gin.Context, err error)
	ConvertError func(err error) error
}

func (e *HttpErrorHandler) Handler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()
		e.IgnoreErrors = append(e.IgnoreErrors, syscall.EPIPE)
		if len(ctx.Errors) == 0 {
			return
		}
		err := ctx.Errors.Last().Err
		if err == nil {
			return
		}
		if e.Logger == nil {
			e.Logger = log.NewLogger()
		}
		e.returnError(ctx, e.convertError(err))

	}
}

func (e *HttpErrorHandler) DefaultConvertError(err error) error {
	for _, er := range e.IgnoreErrors {
		if errors.Is(err, er) {
			e.Logger.Debugf("Ignore error: %s", err.Error())
			e.Logger.Tracef("Trace: %+v", err)
			return nil
		}
	}

	if err.Error() == "EOF" {
		return ErrEmptyRequestParams
	}

	if err, ok := err.(*gin.Error); ok && err.Type == gin.ErrorTypeBind {
		return ErrInvalidRequestParams
	}
	var ute *json.UnmarshalTypeError
	var iue *json.InvalidUnmarshalError

	if errors.As(err, &ute) || errors.As(err, &iue) {
		er := ErrInvalidRequestParams
		er.Message = "invalid request params: " + err.Error()
		return er
	}

	for k, er := range e.CustomErrors {
		if errors.Is(err, k) {
			return er
		}
	}
	return ErrDefaultInternalError
}

func (e *HttpErrorHandler) DefaultReturnError(ctx *gin.Context, err error) {
	if err == nil {
		return
	}
	var resp *ResponseError
	if errors.As(err, &resp) {
		ctx.Negotiate(resp.HTTPCode, gin.Negotiate{
			Offered: []string{gin.MIMEJSON, gin.MIMEXML},
			Data:    resp,
		})
		return
	}
	ctx.Negotiate(http.StatusInternalServerError, gin.Negotiate{
		Offered: []string{gin.MIMEJSON, gin.MIMEXML},
		Data:    ErrDefaultInternalError,
	})
}
func (e *HttpErrorHandler) log() log.Logger {
	if e.Logger == nil {
		e.Logger = log.NewLogger()
	}

	return log.NewLogger()
}

func (e *HttpErrorHandler) returnError(ctx *gin.Context, err error) {
	if e.ReturnError == nil {
		e.ReturnError = e.DefaultReturnError
	}
	e.ReturnError(ctx, err)
}

func (e *HttpErrorHandler) convertError(err error) error {
	if e.ConvertError == nil {
		e.ConvertError = e.DefaultConvertError
	}
	return e.ConvertError(err)

}
