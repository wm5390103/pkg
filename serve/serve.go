package serve

import (
	ginprometheus "github.com/mcuadros/go-gin-prometheus"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/log"
)

type Controller interface {
	InitRoute(routes gin.IRoutes, path string)
}

type Auth interface {
	AuthMiddleware() gin.HandlerFunc
}

type Middleware interface {
	Handler() []gin.HandlerFunc
}

type OneMiddleware interface {
	Handler() gin.HandlerFunc
}

type HTTPServe struct {
	Controllers       map[string]Controller
	Auth              Auth
	BasePath          string
	R                 *gin.Engine
	Listen            string
	Middleware        []Middleware
	ErrorHandler      OneMiddleware
	ErrorNotification OneMiddleware
	Name              string
	CustomerMetrics   []*ginprometheus.Metric

	// array with error that will be not notified
	IgnoreNotificationErrors []error

	// array with error that will be ignored in error handler
	IgnoreHandleErrors []error

	// map[error]*ResponseError that will be used in error handler
	CustomErrors map[error]*ResponseError
	// use ReturnError if you want change  behave to return error in error handler
	ReturnError func(ctx *gin.Context, err error)
	// user ConvertError if you want change  behave to return error in error handler
	ConvertError func(err error) error
}

func (s *HTTPServe) Init() {
	if s.BasePath == "" {
		s.BasePath = "/"
	}
	if s.Listen == "" {
		s.Listen = ":8080"
	}
	if s.R == nil {
		//s.R = gin.Default()
		s.R = gin.New()
		s.R.Use(gin.Recovery())
		s.R.Use(gin.LoggerWithConfig(gin.LoggerConfig{
			SkipPaths: []string{"/ping", "/metrics", "/healthz"},
		}))
	}

	if s.Name == "" {
		if s.BasePath == "/" {
			s.Name = "http_service"
		}
		s.Name = strings.ReplaceAll(strings.Trim(strings.ToLower(s.BasePath), "/"), "/", "_")
	}
	if s.CustomerMetrics == nil {
		s.CustomerMetrics = make([]*ginprometheus.Metric, 0)
	}
	p := ginprometheus.NewPrometheus(s.Name, s.CustomerMetrics)
	p.Use(s.R)
}
func (s *HTTPServe) InitRoute() {
	group := s.R.Group(s.BasePath)

	if s.ErrorHandler == nil {
		s.ErrorHandler = &HttpErrorHandler{
			CustomErrors: s.CustomErrors,
			IgnoreErrors: s.IgnoreHandleErrors,
			ReturnError:  s.ReturnError,
			ConvertError: s.ConvertError,
		}
	}
	group.Use(s.ErrorHandler.Handler())

	if s.ErrorNotification == nil {
		s.ErrorNotification = NewDefaultErrbitNotificator(s.IgnoreNotificationErrors)
	}
	group.Use(s.ErrorNotification.Handler())

	if s.Auth != nil {
		group.Use(s.Auth.AuthMiddleware())
	}

	for _, middleware := range s.Middleware {
		group.Use(middleware.Handler()...)
	}

	for p, c := range s.Controllers {
		if c != nil {
			c.InitRoute(group, p)
		}
	}
	s.R.GET("/ping", func(context *gin.Context) {
		context.String(http.StatusOK, "pong")
	})
}

func (s *HTTPServe) Run() error {
	s.InitRoute()
	err := s.R.Run(s.Listen)
	if err != nil {
		return err
	}
	return nil
}

func (s *HTTPServe) Start() {
	s.Init()
	log.Panic(s.Run())
}

func (s *HTTPServe) Stop() {
}
