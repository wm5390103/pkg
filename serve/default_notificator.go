package serve

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/errors/notificator"
	"gitlab.wm.local/wm/pkg/log"
	"net/http"
	"os"
	"strconv"
)

type emptyNotificator struct {
}

func (e emptyNotificator) Notify(err interface{}, r *http.Request) {
	log.Errorf("notificator is nil")
	log.Errorf("%s", err)
}

type DefaultNotificator struct {
	Notificator             notificator.Notificator
	IgnoreNotificationError []error
}

func (d *DefaultNotificator) Handler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()
		err := ctx.Errors.Last()
		if err == nil {
			return
		}
		if d.Notificator == nil {
			return
		}
		if err.Error() == "EOF" {
			return
		}
		for _, ignoreError := range d.IgnoreNotificationError {
			if errors.Is(err, ignoreError) {
				return
			}
		}
		d.Notificator.Notify(err.Err, ctx.Request)
	}
}

func NewDefaultErrbitNotificator(ignoreNotificationErrors []error) *DefaultNotificator {

	log.Debugf("notificator is nil try set default notificator")
	ErrbitProjectID, err := strconv.ParseInt(os.Getenv("ERRBIT_PROJECT_ID"), 10, 64)

	if err != nil {
		log.Errorf("ERRBIT_PROJECT_ID is not int64: %s", os.Getenv("ERRBIT_PROJECT_ID"))

	}
	ErrbitKey, ok := os.LookupEnv("ERRBIT_PROJECT_KEY")
	if ok == false {
		log.Errorf("ERRBIT_PROJECT_KEY is not set")
		err = errors.New("ERRBIT_PROJECT_KEY is not set")
	}
	ErrbitHost, ok := os.LookupEnv("ERRBIT_HOST")
	if ok == false {
		log.Errorf("ERRBIT_HOST is not set")
		err = errors.New("ERRBIT_HOST is not set")
	}
	Env, ok := os.LookupEnv("ENV")
	if ok == false {
		log.Errorf("ENV is not set")
		err = errors.New("ENV is not set")
	}
	if err != nil {
		log.Errorf("default notificator error: %s", err)
		return &DefaultNotificator{
			IgnoreNotificationError: ignoreNotificationErrors,
			Notificator:             emptyNotificator{},
		}
	}
	n := notificator.NewErrbitNotificator(ErrbitProjectID, ErrbitKey, ErrbitHost, Env, os.Getenv("ERRBIT_PROXY"))

	return &DefaultNotificator{
		IgnoreNotificationError: ignoreNotificationErrors,
		Notificator:             n,
	}
}

func NewNotificator(n notificator.Notificator, ignoreNotificationErrors []error) *DefaultNotificator {
	return &DefaultNotificator{
		IgnoreNotificationError: ignoreNotificationErrors,
		//IgnoreNotificationTypeErrors: ignoreTypeErrors,
		Notificator: n,
	}
}

func (d *DefaultNotificator) Notify(err interface{}, r *http.Request) {
	if err == nil {
		return
	}
	if d.Notificator == nil {
		return
	}
	d.Notificator.Notify(err, r)
}
