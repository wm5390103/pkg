package serve_tests

import (
	"gitlab.wm.local/wm/pkg/serve"
	"io"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

var srv *serve.HTTPServe

func TestMain(m *testing.M) {
	setup()
	time.Sleep(1 * time.Second)
	code := m.Run()
	teardown()
	os.Exit(code)
}

func setup() {

}

func teardown() {
	srv.Stop()
}

func TestSimpleHttpServe(t *testing.T) {
	srv := &serve.HTTPServe{
		Controllers: map[string]serve.Controller{
			"http_test": &ControllerTest{},
		},
		Auth:       &AuthTest{},
		BasePath:   "test",
		Middleware: nil,

		ErrorHandler: nil,
		IgnoreHandleErrors: []error{
			ErrIgnore,
			ErrIgnore2,
		},
		CustomErrors: map[error]*serve.ResponseError{
			ErrBR:                  &serve.ResponseError{HTTPCode: http.StatusBadRequest, Reason: "bad_request", Message: " webmoney bad request"},
			ErrAuthFailed:          &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "auth_failed", Message: " Unauthorized webmoney auth failed"},
			ErrNotFound:            &serve.ResponseError{HTTPCode: http.StatusNotFound, Reason: "not_found", Message: "webmoney not found"},
			ErrInternalServerError: &serve.ResponseError{HTTPCode: http.StatusInternalServerError, Reason: "internal_server_error", Message: "webmoney internal server error"},
			SpecialError:           &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "special_error", Message: "special error"},
		},
		IgnoreNotificationErrors: []error{
			ErrIgnore,
			ErrIgnore2,
			ErrBR,
			ErrAuthFailed,
			ErrNotFound,
			ErrInternalServerError,
			SpecialError,
		},
		Name: "test_service",
	}
	go func() { srv.Start() }()
	t.Run("GetCheck", func(t *testing.T) {
		res, err := http.Get("http://localhost:8080/test/http_test/check?token=global-token")
		defer res.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 200 {
			t.Errorf("Expected 200, got %d", res.StatusCode)
		}
	})
	t.Run("GetUnauthorized", func(t *testing.T) {
		res, err := http.Get("http://localhost:8080/test/http_test/check")
		defer res.Body.Close()
		if err != nil {
			t.Fatal(err)
		}
		body, err := io.ReadAll(res.Body)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(string(body))
		if !strings.Contains(string(body), "Unauthorized") {
			t.Errorf("Expected Unauthorized, got %s", string(body))
		}

		if res.StatusCode != 401 {
			t.Errorf("Expected 401, got %d", res.StatusCode)
		}

	})
	t.Run("GetInvalidUrl", func(t *testing.T) {
		res, err := http.Get("http://localhost:8080/test/invalid?token=global-token")
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 404 {
			t.Errorf("Expected 404, got %d", res.StatusCode)
		}
	})
	t.Run("PostCheck", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/check?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 200 {
			t.Errorf("Expected 200, got %d", res.StatusCode)
		}
	})
	t.Run("PostUnauthorized", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/check", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		body, err := io.ReadAll(res.Body)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(string(body))
		if !strings.Contains(string(body), "Unauthorized") {
			t.Errorf("Expected Unauthorized, got %s", string(body))
		}

		if res.StatusCode != 401 {
			t.Errorf("Expected 401, got %d", res.StatusCode)
		}
	})
	t.Run("PostInvalidUrl", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/invalid?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 404 {
			t.Errorf("Expected 404, got %d", res.StatusCode)
		}
	})
	t.Run("PostBadRequest", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/bad_request?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 400 {
			t.Errorf("Expected 400, got %d", res.StatusCode)
		}
	})

	t.Run("PostNotFound", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/not_found?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 404 {
			t.Errorf("Expected 404, got %d", res.StatusCode)
		}
	})

	t.Run("PostInternalServerError", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/internal_server_error?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 500 {
			t.Errorf("Expected 500, got %d", res.StatusCode)
		}
	})

	t.Run("GetPrivateUnauthorized", func(t *testing.T) {
		res, err := http.Get("http://localhost:8080/test/private?token=global-token")
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 401 {
			t.Errorf("Expected 401, got %d", res.StatusCode)
		}
	})

	t.Run("GetPrivate", func(t *testing.T) {
		request, err := http.NewRequest("GET", "http://localhost:8080/test/private?token=global-token", nil)
		if err != nil {
			t.Fatal(err)
		}
		request.Header.Set("Authorization", "test-jwt-token")
		res, err := http.DefaultClient.Do(request)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 200 {
			t.Errorf("Expected 200, got %d", res.StatusCode)
		}
	})

	t.Run("IgnoreNotificationError", func(t *testing.T) {
		res, err := http.Get("http://localhost:8080/test/http_test/ignore_error?token=global-token")
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 200 {
			t.Errorf("Expected 200, got %d", res.StatusCode)
		}
	})

	t.Run("IgnoreNotificationError2", func(t *testing.T) {
		res, err := http.Post("http://localhost:8080/test/http_test/ignore2_error?token=global-token", "application/json", nil)
		if err != nil {
			t.Fatal(err)
		}
		if res.StatusCode != 200 {
			t.Errorf("Expected 200, got %d", res.StatusCode)
		}
	})
}
