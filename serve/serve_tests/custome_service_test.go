package serve_tests

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/serve"
	"io"
	"net/http"
	"strings"
	"testing"
)

func TestCustomeService(t *testing.T) {
	srv := &serve.HTTPServe{
		Controllers: map[string]serve.Controller{
			"http_test": &ControllerTest{},
		},
		Auth:       &AuthTest{},
		BasePath:   "test2",
		Middleware: nil,
		IgnoreHandleErrors: []error{
			ErrIgnore,
			ErrIgnore2,
		},
		CustomErrors: map[error]*serve.ResponseError{
			ErrBR:                  &serve.ResponseError{HTTPCode: http.StatusBadRequest, Reason: "bad_request", Message: " webmoney bad request"},
			ErrAuthFailed:          &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "auth_failed", Message: " Unauthorized webmoney auth failed"},
			ErrNotFound:            &serve.ResponseError{HTTPCode: http.StatusNotFound, Reason: "not_found", Message: "webmoney not found"},
			ErrInternalServerError: &serve.ResponseError{HTTPCode: http.StatusInternalServerError, Reason: "internal_server_error", Message: "webmoney internal server error"},
			SpecialError:           &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "special_error", Message: "special error"},
		},
		IgnoreNotificationErrors: []error{
			ErrIgnore,
			ErrIgnore2,
			ErrBR,
			ErrAuthFailed,
			ErrNotFound,
			ErrInternalServerError,
			SpecialError,
		},
		Name: "test_service",
		ConvertError: func(err error) error {
			if errors.Is(err, ErrBR) {
				return MyAuthError{Msg: "auth failed", Err: errors.New("custom bad request")}
			}
			return err
		},
		ReturnError: func(ctx *gin.Context, err error) {
			accept := ctx.GetHeader("Content-Type")
			returnCode := http.StatusBadRequest
			message := err.Error()
			var er *serve.ResponseError
			if errors.As(err, &er) {
				returnCode = er.HTTPCode
				message = er.Message
			}
			if accept == "application/json" {
				ctx.JSON(returnCode, gin.H{"error": message})
			} else {
				ctx.String(returnCode, message)
			}
		},
	}
	go func() { srv.Start() }()

	t.Run("CheckReturnError", func(t *testing.T) {
		resp, err := http.Post("http://localhost:8080/test2/http_test/not_found?token=global-token", "application/json", strings.NewReader(`{"name": "test"}`))
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != 404 {
			t.Errorf("Expected 404, got %d", resp.StatusCode)
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}
		if 0 != strings.Compare(string(body), "{\"error\":\"webmoney not found\"}") {
			t.Errorf("Expected webmonoey not found, got %s", string(body))
		}

	})
	t.Run("CheckReturnError2", func(t *testing.T) {
		resp, err := http.Post("http://localhost:8080/test2/http_test/not_found?token=global-token", "text/html", strings.NewReader(`{"name": "test"}`))
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != 404 {
			t.Errorf("Expected 401, got %d", resp.StatusCode)
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}
		if 0 != strings.Compare(string(body), "webmoney not found") {
			t.Errorf("Expected auth failed, got %s", string(body))
		}
	})
}
