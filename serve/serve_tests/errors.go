package serve_tests

import (
	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/errors"
	"gitlab.wm.local/wm/pkg/log"
	"gitlab.wm.local/wm/pkg/serve"
	"net/http"
)

var (
	ErrBR                  = errors.New("bad_request")
	ErrAuthFailed          = errors.New("auth_failed")
	ErrNotFound            = errors.New("not_found")
	ErrInternalServerError = errors.New("internal_server_error")
	SpecialError           = errors.New("special_error")
	ErrIgnore              = errors.New("ignore")
	ErrIgnore2             = errors.New("ignore2")
)

type MyFuckenError struct {
}

func (e MyFuckenError) Error() string {
	return "fuck you"
}

type MyAuthError struct {
	Msg string
	Err error
}

func (e MyAuthError) Unwrap() error {
	return e.Err
}

func (e MyAuthError) Error() string {
	return "auth failed"
}

func (e MyAuthError) HTTPCode() int {
	return http.StatusUnauthorized
}

var customErrors = map[error]*serve.ResponseError{
	ErrBR:                  &serve.ResponseError{HTTPCode: http.StatusBadRequest, Reason: "bad_request", Message: " webmoney bad request"},
	ErrAuthFailed:          &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "auth_failed", Message: "webmoney auth failed"},
	ErrNotFound:            &serve.ResponseError{HTTPCode: http.StatusNotFound, Reason: "not_found", Message: "webmoney not found"},
	ErrInternalServerError: &serve.ResponseError{HTTPCode: http.StatusInternalServerError, Reason: "internal_server_error", Message: "webmoney internal server error"},
	SpecialError:           &serve.ResponseError{HTTPCode: http.StatusUnauthorized, Reason: "special_error", Message: "special error"},
}

type MySpecialError struct {
}

func (m MySpecialError) Error() string {
	//TODO implement me
	panic("implement me")
}

var mse *MySpecialError
var mae *MyAuthError
var errorHandler = &serve.HttpErrorHandler{
	CustomErrors: customErrors,
	IgnoreErrors: []error{
		ErrIgnore,
		ErrIgnore2,
	},
	ConvertError: func(err error) error {
		if errors.Is(err, SpecialError) {
			return MyFuckenError{}
		}

		return err
	},
	ReturnError: func(ctx *gin.Context, err error) {
		var mfe *MyFuckenError
		if errors.As(err, &mfe) { // works only if second parameter is  type *MyFuckenError
			//ctx.Status(499)
			ctx.String(499, "fuck you")
		}
		handler := &serve.HttpErrorHandler{}
		handler.DefaultReturnError(ctx, err)
	},
}

type errNof struct {
}

func (e errNof) Handler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()
		err := ctx.Errors.Last()
		log.Printf("Notification stacktrace %+v", err)
	}
}
