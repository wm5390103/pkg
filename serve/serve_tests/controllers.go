package serve_tests

import (
	"github.com/gin-gonic/gin"
	"gitlab.wm.local/wm/pkg/serve"
	"net/http"
	"path"
)

type ControllerTest struct {
	serve.BaseController
}

func (c *ControllerTest) InitRoute(routes gin.IRoutes, p string) {
	c.GET(routes, path.Join(p, "/check"), c.Check)
	c.POST(routes, path.Join(p, "/check"), c.Check)
	c.PUT(routes, path.Join(p, "/check"), c.Check)
	c.PATCH(routes, path.Join(p, "/check"), c.Check)
	c.DELETE(routes, path.Join(p, "/check"), c.Check)
	c.GET(routes, path.Join(p, "/bad_request"), c.BadRequest)
	c.POST(routes, path.Join(p, "/bad_request"), c.BadRequest)
	c.GET(routes, path.Join(p, "/not_found"), c.NotFound)
	c.POST(routes, path.Join(p, "/not_found"), c.NotFound)
	c.GET(routes, path.Join(p, "/internal_server_error"), c.InternalServerError)
	c.POST(routes, path.Join(p, "/internal_server_error"), c.InternalServerError)
	c.GET(routes, path.Join(p, "/ignore_error"), c.IgnoreError)
	c.POST(routes, path.Join(p, "/ignore2_error"), c.Ignore2Error)
	routes.GET("/private", c.AuthMiddleware, c.CheckAuth)
}

func (c *ControllerTest) Check(ctx *gin.Context) error {
	ctx.JSON(200, gin.H{"status": "ok"})
	return nil
}

func (c *ControllerTest) BadRequest(ctx *gin.Context) error {
	return ErrBR
}

func (c *ControllerTest) NotFound(ctx *gin.Context) error {
	return ErrNotFound
}

func (c *ControllerTest) InternalServerError(ctx *gin.Context) error {
	return ErrInternalServerError
}
func (c *ControllerTest) IgnoreError(ctx *gin.Context) error {
	return ErrIgnore
}
func (c *ControllerTest) Ignore2Error(ctx *gin.Context) error {
	return ErrIgnore2
}

func (c *ControllerTest) AuthMiddleware(context *gin.Context) {
	//context.Header("Authorization", "test-jwt-token")
	context.GetHeader("Authorization")
	if context.GetHeader("Authorization") != "test-jwt-token" {
		context.Error(ErrAuthFailed)
		context.Abort()
		return
	}
	context.Next()
}

func (c *ControllerTest) CheckAuth(context *gin.Context) {
	context.String(200, "pong")
}

type AuthTest struct {
}

func (a *AuthTest) AuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token, ok := ctx.GetQuery("token")
		if !ok || token != "global-token" {
			_ = ctx.Error(&MyAuthError{Msg: "auth failed", Err: ErrAuthFailed})

			ctx.Status(http.StatusUnauthorized)
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}
