// unit tests
package queuer

import (
	"os"
	"testing"
)

func TestConfig_MergeDefaults(t *testing.T) {
	t.Run("merge with default values without env", func(t *testing.T) {
		os.Unsetenv("AMQP_URL")
		c := &Config{
			DSN: "test@test",
		}
		err := c.MergeDefaults()
		if err != nil {
			t.Fatal("Error " + err.Error())
		}
		if c.DSN != "test@test" {
			t.Errorf("DSN should be test@test")
		}
		//assert.Equal(t, "test@test", c.DSN)

		//assert.Equal(t, "direct", c.ExchangeOptions.Kind)
		if c.ExchangeOptions.Durable != true {
			t.Errorf("Durable should be true")
		}
		if c.ExchangeOptions.AutoDelete != false {
			t.Errorf("AutoDelete should be false")
		}
	})

	t.Run("merge exchange option ", func(t *testing.T) {
		c := &Config{
			ExchangeOptions: ExchangeOptions{
				Kind:       "top",
				Durable:    true,
				AutoDelete: true,
			},
		}
		err := c.MergeDefaults()
		if err != nil {
			return
		}
		//assert.Equal(t, "top", c.ExchangeOptions.Kind)
		if c.ExchangeOptions.Kind != "top" {
			t.Errorf("Kind should be top")
		}
		//assert.Equal(t, ExchangeOptionsDefaults.NoWait, c.ExchangeOptions.NoWait)
	})
}
