package queuer

import (
	"log"
	"time"
)

func StartSimpleWorker(c *Config, handler WorkerHandler, rej WorkerRejector) error {
	worker, err := NewWorker(c, handler, rej)
	if err != nil {
		return err
	}
	go worker.Run()
	log.Printf("Start simple worker  Exchange: %s QueName: %s RoutKey: %s", c.Exchange, c.QueName, c.RoutKey)
	for {
		select {
		case err := <-worker.Fatal:
			worker.Stop()
			log.Println("Sleep to 5 second before fail")
			time.Sleep(5 * time.Second)
			log.Fatalf("Fatal error:  %s", err)
		case err := <-worker.Errors:
			log.Printf("Worker return Error %s", err)
		case done := <-worker.Done:
			log.Printf("Done from  routKey %s, exchange %s body: %s", done.RoutingKey, done.Exchange, string(done.Body))

		}
	}
}
