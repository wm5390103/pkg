module gitlab.wm.local/wm/pkg/queuer

go 1.19

require (
	github.com/rabbitmq/amqp091-go v1.10.0
	gitlab.wm.local/wm/pkg/errors v1.3.1

)
