module gitlab.wm.local/wm/pkg/errors/notificator

go 1.21

require (
	github.com/airbrake/gobrake/v5 v5.6.1
	gitlab.wm.local/wm/pkg/errors v1.3.1
	gitlab.wm.local/wm/pkg/log v0.7.4
)

require (
	github.com/caio/go-tdigest/v4 v4.0.1 // indirect
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.24.0 // indirect
)
