package errors_test

import (
	"fmt"
	"gitlab.wm.local/wm/pkg/errors"
	"testing"
)

func ExampleNew() {
	err := errors.New("some error")

	fmt.Printf("%+v", err)
}

func TestNew(t *testing.T) {
	ExampleNew()
}

func TestHandler(t *testing.T) {
	err := service("return error")
	if err != nil {
		fmt.Printf("%+v \n", err)
	}

}

func service(s string) error {
	if s != "" {
		err := repository(s)
		if err != nil {
			return err
		}
	}
	return nil
}

func repository(s string) error {
	if s != "" {
		return errors.New(s)
	}
	return nil
}
